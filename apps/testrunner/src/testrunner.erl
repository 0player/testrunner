-module(testrunner).

%% API
-export([start/0, stop/0, run_sync/2]).

start() ->
  application:start(testrunner).

stop() ->
  application:stop(testrunner).

run_sync(CommandText, TimeoutInMs) ->
  case gen_server:call({global, testrunner_master}, {run_command, CommandText, TimeoutInMs}, TimeoutInMs) of
    {ok, Ref} ->
      receive
        {Ref, Result} -> Result
      after TimeoutInMs -> timeout
      end;
    Error -> Error
  end.
