%%%-------------------------------------------------------------------
%% @doc testrunner public API
%% @end
%%%-------------------------------------------------------------------

-module(testrunner_app).

-behaviour(application).

-export([start/2, stop/1]).

start(normal, _StartArgs) ->
    testrunner_sup:start_link([]);
start({failover, _}, _StartArgs) ->
    testrunner_sup:start_link([]);
start({takeover, Node}, _StartArgs) ->
    testrunner_sup:start_link(testrunner_master:takeover(Node)).
stop(_State) ->
    ok.

%% internal functions
