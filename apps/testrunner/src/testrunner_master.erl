%%%-------------------------------------------------------------------
%%% @author alex
%%% @copyright (C) 2020, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 22. May 2020 15:14
%%%-------------------------------------------------------------------
-module(testrunner_master).
-author("alex").

-behaviour(gen_server).

%% API
-export([start_link/1, takeover/1]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(state, {
  table :: ets:tab(),
  overseers :: [pid()],
  timer :: reference() | undefined
}).

%%%===================================================================
%%% API
%%%===================================================================

start_link(TakeoverInit) ->
  gen_server:start_link({global, ?SERVER}, ?MODULE, TakeoverInit, []).

takeover(Node) ->
  try
      gen_server:call({?SERVER, Node}, takeover, 5000)
  of
    {takeover, Table} ->
      Table
  catch
      exit:_ -> []
  end.

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

init(Table) ->
  Tab = ets:new(timing_state, [ordered_set, private]),
  Offset = erlang:time_offset(),
  ets:insert(Tab, [
    {{Timeout - Offset, monitor(process, WorkerPid)}, Pid, WorkerPid} || {Timeout, Pid, WorkerPid} <- Table
  ]),
  {ok, rearm_timer(#state{table = Tab, overseers = []})}.

handle_call({run_command, _InputData, _Timeout}, _From, State = #state{overseers = []}) ->
  {reply, {error, no_workers_available}, State};
handle_call({run_command, InputData, Timeout}, _From = {Pid, _Tag}, State = #state{table = Tab, overseers = Overseers}) ->
  CurrentTime = erlang:monotonic_time(millisecond),
  case send_to_alive_node(Overseers, InputData) of
    {[], _} ->
      {reply, {error, no_workers_available}, State#state{overseers =[]}};
    {Overseers1, {MonitorRef, WorkerPid}} ->
      ets:insert_new(Tab, {{Timeout + CurrentTime, MonitorRef}, Pid, WorkerPid}),
      {reply, {ok, MonitorRef}, rearm_timer(State#state{overseers = Overseers1})}
  end;
handle_call(takeover, _From, State = #state{table = Tab}) ->
  Offset = erlang:time_offset(millisecond),
  {stop, normal, {takeover,
    [{Timeout + Offset, Pid, WorkerPid} || {{Timeout, _}, Pid, WorkerPid} <- ets:tab2list(Tab)]}, State}.


handle_cast({join, Pid}, State = #state{overseers = Overseers}) ->
  {noreply, State#state{overseers = [Pid | Overseers]}};
handle_cast({result, WorkerPid, Result}, State = #state{table = Tab}) ->
  case ets:match_object(Tab, {'_', '_', WorkerPid}, 1) of
    '$end_of_table' -> {noreply, State}; %huh.
    {[{Key = {_, Ref}, Pid, _}], _Cont} ->
      Pid ! {Ref, Result},
      demonitor(Ref, [flush]),
      ets:delete(Tab, Key),
      {noreply, rearm_timer(State)}
  end.

handle_info({'DOWN', _, process, _, noconnection}, State) ->
  % might be a transient error, don't give up just yet.
  {noreply, State};
handle_info({'DOWN', MonRef, process, Pid, Reason}, State = #state{table = Tab}) ->
  case ets:match_object(Tab, {{'_', MonRef}, '_', Pid}, 1) of
    '$end_of_table' -> {noreply, State};
    {[{Key, Pid, _}], _Cont} ->
      Pid ! {MonRef, {'EXIT', Reason}},
      ets:delete(Tab, Key),
      {noreply, rearm_timer(State)}
  end;
handle_info({timeout, TimerRef, Key}, State = #state{table = Tab, timer = TimerRef}) ->
  case ets:lookup(Tab, Key) of
    [] -> {noreply, rearm_timer(State)};
    [{{_, MonRef}, Pid, WorkerPid}] ->
      Pid ! {MonRef, timeout},
      exit(WorkerPid, timeout),
      ets:delete(Tab, Key),
      {noreply, rearm_timer(State)}
  end;
handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

send_to_alive_node([], _Term) -> {[], nil};
send_to_alive_node([Overseer | Overseers], Term) ->
  try gen_server:call(Overseer, {spawn, Term}) of
    {ok, Pid} when is_pid(Pid) ->
      MoniRef = monitor(process, Pid),
      {Overseers ++ [Overseer], {MoniRef, Pid}}
  catch
    exit:_ -> send_to_alive_node(Overseers, Term)
  end.

rearm_timer(State = #state{timer = Ref, table = Tab}) ->
  State#state{timer = rearm_timer(Tab, Ref)}.

rearm_timer(Tab, OldTimerRef) ->
  case OldTimerRef of
    undefined -> ok;
    _ -> catch erlang:cancel_timer(OldTimerRef)
  end,
  case ets:first(Tab) of
    {Timeout, _} = Key ->
      erlang:start_timer(Timeout, self(), Key, [{abs, true}]);
    '$end_of_table' -> undefined
  end.
