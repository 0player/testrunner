%%%-------------------------------------------------------------------
%% @doc testrunner top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(testrunner_sup).

-behaviour(supervisor).

-export([start_link/1]).

-export([init/1]).

-define(SERVER, ?MODULE).

start_link(TakeoverInit) ->
    supervisor:start_link({global, ?SERVER}, ?MODULE, TakeoverInit).

%% sup_flags() = #{strategy => strategy(),         % optional
%%                 intensity => non_neg_integer(), % optional
%%                 period => pos_integer()}        % optional
%% child_spec() = #{id => child_id(),       % mandatory
%%                  start => mfargs(),      % mandatory
%%                  restart => restart(),   % optional
%%                  shutdown => shutdown(), % optional
%%                  type => worker(),       % optional
%%                  modules => modules()}   % optional
init(TakeoverInit) ->
    SupFlags = #{strategy => one_for_all,
                 intensity => 1,
                 period => 5},
    ChildSpecs = [
      #{
        id => master,
        start => {testrunner_master, start_link, [TakeoverInit]},
        restart => permanent
      }
    ],
    {ok, {SupFlags, ChildSpecs}}.

%% internal functions
