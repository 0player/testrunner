-module(testrunner_worker).

%% API
-export([start/0, stop/0, spawn/1]).

start() ->
  application:start(testrunner_worker).

stop() ->
  application:stop(testrunner_worker).

spawn(ProgramText) ->
  testrunner_worker_proc_sup:spawn(ProgramText).
