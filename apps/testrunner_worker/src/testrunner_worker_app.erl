%%%-------------------------------------------------------------------
%% @doc testrunner_worker public API
%% @end
%%%-------------------------------------------------------------------

-module(testrunner_worker_app).

-behaviour(application).

-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
    testrunner_worker_sup:start_link().

stop(_State) ->
    ok.

%% internal functions
