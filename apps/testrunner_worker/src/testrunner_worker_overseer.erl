-module(testrunner_worker_overseer).

-behaviour(gen_server).

%% API
-export([start_link/0, gather_nodes/1]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

%%%===================================================================
%%% API
%%%===================================================================

start_link() ->
  gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

gather_nodes(Nodes) ->
  {_, BadNodes} = gen_server:multi_call(Nodes, ?SERVER, ping),
  Nodes -- BadNodes.

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

init([]) ->
  case global:whereis_name(testrunner_master) of
    undefined -> {stop, noconnection};
    Pid ->
      gen_server:cast(Pid, {join, self()}),
      {ok, monitor(process, Pid)}
  end.

handle_call(ping, _From = {Pid, _}, OldMRef) ->
  demonitor(OldMRef, [flush]),
  {reply, pong, monitor(process, Pid)};
handle_call({spawn, Text}, _From, State) ->
  {reply, testrunner_worker:spawn(Text), State}.

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info({'DOWN', MonRef, process, _Pid, _Reason}, MonRef) ->
  {stop, noconnection, undefined};  % in case of network failure, try restarting and re-joining the new master.
handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
