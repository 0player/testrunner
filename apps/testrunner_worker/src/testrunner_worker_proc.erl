-module(testrunner_worker_proc).

%% API
-export([start_link/1]).

-export([init_proc/1]).

start_link(ProgramText) ->
  proc_lib:start_link(?MODULE, init_proc, [ProgramText], 5000).

init_proc(ProgramText) ->
  Port = open_port({spawn_executable, "/bin/sh"}, [
    {args, ["-s"]},
    stream, in, out, use_stdio, stderr_to_stdout, exit_status
  ]),
  true = port_command(Port, [
    "(\n",
    ProgramText,
    "\n) < /dev/null; return 0\n"
  ]),
  proc_lib:init_ack({ok, self()}),
  Reply = collect_data(Port, []),
  catch port_close(Port),
  gen_server:cast({global, testrunner_master}, {result, self(), iolist_to_binary([Reply])}).

collect_data(Port, SoFar) ->
  receive
    {Port, {data, Data}} ->
      collect_data(Port, [SoFar, Data]);
    {Port, {exit_status, _}} ->
      SoFar;
    {'EXIT', _PidOrPort, _Reason} ->
      SoFar
  end.