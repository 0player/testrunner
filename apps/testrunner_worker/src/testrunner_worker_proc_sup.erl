%%%-------------------------------------------------------------------
%% @doc testrunner_worker top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(testrunner_worker_proc_sup).

-behaviour(supervisor).

-export([start_link/0, spawn/1]).

-export([init/1]).

-define(SERVER, ?MODULE).

start_link() ->
  supervisor:start_link({local, ?SERVER}, ?MODULE, []).

spawn(ProgramText) ->
  supervisor:start_child(?SERVER, [ProgramText]).

init([]) ->
  SupFlags = #{strategy => simple_one_for_one,
    intensity => 0,
    period => 1},
  ChildSpecs = [
    #{
      id => worker_proc,
      start => {testrunner_worker_proc, start_link, []},
      restart => temporary,
      shutdown => brutal_kill
    }
  ],
  {ok, {SupFlags, ChildSpecs}}.

%% internal functions
