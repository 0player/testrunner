-module(testrunner_worker_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%%===================================================================
%%% API functions
%%%===================================================================
start_link() ->
  supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

init([]) ->
  SupFlags = #{
    strategy => rest_for_one,
    intensity => 1,
    period => 5
  },

  ChildSpecs = [
    #{
      id => proc_sup,
      start => {testrunner_worker_proc_sup, start_link, []},
      type => supervisor
    },
    #{
      id => overseer,
      start => {testrunner_worker_overseer, start_link, []}
    }
  ],

  {ok, {SupFlags, ChildSpecs}}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
