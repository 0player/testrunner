#!/bin/sh
set -e

./rebar3 compile || {
    echo Requires Erlang to be installed on your system. Try snap install erlang or apt install erlang.
    exit 1
}

echo 'To test, try `testrunner:run_sync("CommandText", TimeoutInMs).'
echo 'Example: testrunner:run_sync("echo Hello world!", 1000).'
echo 'Example: testrunner:run_sync("cat /etc/passwd", 1000).'
./rebar3 shell --sname test
